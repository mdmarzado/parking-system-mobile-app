export const RecentTransactionReducer = (prevState, action) => {
  switch (action.type) {
    case "ADD_TRANSACTION":
      return [
        ...prevState,
        {
          gateEntered: action.gateEntered,
          dateEntered: action.dateEntered,
          gateExit: action.gateExit,
          dateExit: action.dateExit,
          slotID: action.slotID,
          slotName: action.slotName,
          slotSize: action.slotSize,
          vehicleNo: action.vehicleNo,
          vehicleSize: action.vehicleSize,
        },
      ];

    default:
      return prevState;
  }
};
