export const EntryPointsReducer = (prevState, action) => {
  switch (action.type) {
    case "ADD_ENTRY_POINT":
      return [...prevState, { id: action.id, name: action.name }];
    case "DELETE_ENTRY_POINT":
      return prevState.filter((entryPoint) => entryPoint.id !== action.id);
    default:
      return prevState;
  }
};
