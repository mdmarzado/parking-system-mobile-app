export const SlotReducer = (prevState, action) => {
  switch (action.type) {
    case "ADD_SLOT":
      return;
    case "UPDATE_SLOT":
      let registered = false;
      let newState = prevState.map((slot) => {
        if (slot.vehicleNo === "" && !registered) {
          if (action.vehicleSize === "Small") {
            registered = true;
            return {
              ...slot,
              vehicleNo: action.vehicleNo,
              vehicleSize: action.vehicleSize,
              date: action.date,
              gateID: action.gateID,
            };
          } else if (action.vehicleSize === "Medium") {
            if (slot.size === "Medium" || slot.size === "Large") {
              registered = true;
              return {
                ...slot,
                vehicleNo: action.vehicleNo,
                vehicleSize: action.vehicleSize,
                date: action.date,
                gateID: action.gateID,
              };
            }
          } else {
            if (slot.size === "Large") {
              registered = true;
              return {
                ...slot,
                vehicleNo: action.vehicleNo,
                vehicleSize: action.vehicleSize,
                date: action.date,
                gateID: action.gateID,
              };
            }
          }
        }
        return slot;
      });

      return newState;
    case "DELETE_SLOT_VEHICLE":
      return prevState.map((slot) => {
        if (slot.id === action.id) {
          return {
            ...slot,
            vehicleNo: "",
            vehicleSize: "",
            date: "",
            gateID: null,
          };
        } else {
          return slot;
        }
      });
    default:
      return prevState;
  }
};
