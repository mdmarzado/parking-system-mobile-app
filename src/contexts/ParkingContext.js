import React, { createContext, useReducer, useState } from "react";

import { EntryPointsReducer } from "../reducers/EntryPointsReducer";
import { SlotReducer } from "../reducers/SlotReducer";
import { RecentTransactionReducer } from "../reducers/RecentTransactionReducer";

export const ParkingContext = createContext({});

const ParkingContextProvider = ({ children }) => {
  const [currentGate, setCurrentGate] = useState(null);

  const entryPointInitialState = [
    { id: 1, name: "Gate 1" },
    { id: 2, name: "Gate 2" },
    { id: 3, name: "Gate 3" },
  ];

  const slotInitialState = [
    {
      id: 101,
      name: "S101",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 102,
      name: "S102",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 103,
      name: "S103",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 104,
      name: "S104",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 105,
      name: "S105",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 106,
      name: "S106",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 107,
      name: "S107",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 108,
      name: "S108",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 109,
      name: "S109",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 110,
      name: "S110",
      size: "Small",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 201,
      name: "M201",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 202,
      name: "M202",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 203,
      name: "M203",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 204,
      name: "M204",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 205,
      name: "M205",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 206,
      name: "M206",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 207,
      name: "M207",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 208,
      name: "M208",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 209,
      name: "M209",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 210,
      name: "M210",
      size: "Medium",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 301,
      name: "L301",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 302,
      name: "L302",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 303,
      name: "L303",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 304,
      name: "L304",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 305,
      name: "L305",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 306,
      name: "L306",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 307,
      name: "L307",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 308,
      name: "L308",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 309,
      name: "L309",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
    {
      id: 310,
      name: "L310",
      size: "Large",
      vehicleNo: "",
      vehicleSize: "",
      date: "",
      gateID: null,
    },
  ];

  const recentTransactionInitialState = [];

  const [entryPointState, entryPointDispatch] = useReducer(
    EntryPointsReducer,
    entryPointInitialState
  );

  const [slotState, slotDispatch] = useReducer(SlotReducer, slotInitialState);

  const [recentTransactionState, recentTransactionDispatch] = useReducer(
    RecentTransactionReducer,
    recentTransactionInitialState
  );

  return (
    <ParkingContext.Provider
      value={{
        currentGate,
        setCurrentGate,
        entryPointState,
        entryPointDispatch,
        slotState,
        slotDispatch,
        recentTransactionState,
        recentTransactionDispatch,
      }}
    >
      {children}
    </ParkingContext.Provider>
  );
};

export default ParkingContextProvider;
