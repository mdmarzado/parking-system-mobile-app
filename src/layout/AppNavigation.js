import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import HomeScreen from "../screens/HomeScreen";
import EntryPointFormScreen from "../screens/EntryPointFormScreen";
import ParkingScreen from "../screens/ParkingScreen";
import RegistrationScreen from "../screens/RegistrationScreen";
import CheckoutScreen from "../screens/CheckoutScreen";
import CheckoutFormScreen from "../screens/CheckoutFormScreen";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function TabNavigation() {
  return (
    <Tab.Navigator initialRouteName="Parking">
      <Tab.Screen
        name="Parking"
        component={ParkingScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="car-multiple"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Registration"
        component={RegistrationScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="content-save"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Checkout Navigation"
        component={CheckoutScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="exit-to-app"
              color={color}
              size={size}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

function AppNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen
          name="Entry Point Form"
          component={EntryPointFormScreen}
        />
        <Stack.Screen
          name="TabNavigation"
          component={TabNavigation}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Checkout Form" component={CheckoutFormScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigation;
