import React, { useContext } from "react";
import { ScrollView } from "react-native";
import { List } from "react-native-paper";

import { ParkingContext } from "../contexts/ParkingContext";

const ParkingScreen = () => {
  const { entryPointState, slotState } = useContext(ParkingContext);

  function getGateName(id) {
    return entryPointState.filter((gate) => gate.id === id)[0].name;
  }

  return (
    <ScrollView>
      <List.Section>
        <List.Accordion title="Occupied" expanded>
          {slotState
            .filter((occupied) => occupied.vehicleNo !== "")
            .map((slot) => (
              <List.Item
                style={{
                  backgroundColor: "#fafafa",
                  margin: 8,
                }}
                key={slot.id}
                title={slot.name}
                description={`${getGateName(slot.gateID)} - ${slot.vehicleNo} - ${
                  slot.vehicleSize
                } - ${slot.date}`}
              />
            ))}
        </List.Accordion>
        <List.Accordion title="Vacant" expanded>
          {slotState
            .filter((occupied) => occupied.vehicleNo === "")
            .map((slot) => (
              <List.Item
                style={{
                  backgroundColor: "#fafafa",
                  margin: 8,
                }}
                key={slot.id}
                title={slot.name}
              />
            ))}
        </List.Accordion>
      </List.Section>
    </ScrollView>
  );
};

export default ParkingScreen;
