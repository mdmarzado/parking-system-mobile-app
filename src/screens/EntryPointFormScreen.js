import React, { useContext, useState } from "react";
import { View } from "react-native";
import { Button, Text, TextInput } from "react-native-paper";

import { ParkingContext } from "../contexts/ParkingContext";

const EntryPointFormScreen = ({ navigation }) => {
  const { entryPointDispatch } = useContext(ParkingContext);
  const [entryPointID, setEntryPointID] = useState(null);
  const [entryPointName, setEntryPointName] = useState("");

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text variant="headlineSmall" style={{ marginBottom: 32 }}>
        Entry Point Details
      </Text>
      <TextInput
        style={{ width: "100%" }}
        mode="outlined"
        label="Entry Point ID"
        value={entryPointID}
        onChangeText={(text) => setEntryPointID(text)}
      />
      <TextInput
        style={{ width: "100%" }}
        mode="outlined"
        label="Entry Point Name"
        value={entryPointName}
        onChangeText={(text) => setEntryPointName(text)}
      />
      <Button
        disabled={entryPointID === null || entryPointName === ""}
        mode="contained"
        onPress={() => {
          entryPointDispatch({
            type: "ADD_ENTRY_POINT",
            id: entryPointID,
            name: entryPointName,
          });

          setEntryPointID(null);
          setEntryPointName("");

          navigation.goBack();
        }}
        style={{ width: "100%", padding: 8, marginTop: 32 }}
      >
        Add Entry Point
      </Button>
      <Button
        mode="outlined"
        onPress={() => {
          setEntryPointID(null);
          setEntryPointName("");
        }}
        style={{ width: "100%", padding: 8, marginTop: 8 }}
      >
        Clear
      </Button>
    </View>
  );
};

export default EntryPointFormScreen;
