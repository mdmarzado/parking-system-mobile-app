import React, { useContext, useState } from "react";
import { useFocusEffect } from "@react-navigation/native";
import { View } from "react-native";
import { Button, Text, TextInput, RadioButton } from "react-native-paper";
const moment = require("moment");

import { ParkingContext } from "../contexts/ParkingContext";

const RegistrationScreen = ({ navigation }) => {
  const { currentGate, slotDispatch, recentTransactionState } =
    useContext(ParkingContext);
  const [vehicleNo, setVehicleNo] = useState("");
  const [vehicleSize, setVehicleSize] = useState("Small");
  const [date, setDate] = useState(moment().format("DD/MM/YYYY hh:mm:ss A"));

  useFocusEffect(
    React.useCallback(() => {
      setDate(moment().format("DD/MM/YYYY hh:mm:ss A"));

      return () => null;
    }, [])
  );

  function onSubmit() {
    const recentResult = recentTransactionState.filter(
      (recent) => recent.vehicleNo === vehicleNo
    );
    
    if (recentResult.length > 0) {
      const lastElement = recentResult.pop();

      const recentEnteredDate = moment(
        lastElement.dateEntered,
        "DD/MM/YYYY hh:mm:ss A"
      );
      const newEnteredDate = moment(date, "DD/MM/YYYY hh:mm:ss A");

      const seconds = newEnteredDate.diff(recentEnteredDate, "seconds");
      const calculatedHours = seconds / 3600;

      if (calculatedHours < 1) {
        slotDispatch({
          type: "UPDATE_SLOT",
          vehicleNo: lastElement.vehicleNo,
          vehicleSize: lastElement.vehicleSize,
          date: lastElement.dateEntered,
          gateID: currentGate,
        });
      } else {
        slotDispatch({
          type: "UPDATE_SLOT",
          vehicleNo: vehicleNo,
          vehicleSize: vehicleSize,
          date: date,
          gateID: currentGate,
        });
      }
    } else {
      slotDispatch({
        type: "UPDATE_SLOT",
        vehicleNo: vehicleNo,
        vehicleSize: vehicleSize,
        date: date,
        gateID: currentGate,
      });
    }

    setVehicleNo("");
    setVehicleSize("Small");
    setDate(moment().format("DD/MM/YYYY hh:mm:ss A"));
  }

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text variant="headlineSmall" style={{ marginBottom: 32 }}>
        Registration Details
      </Text>
      <TextInput
        style={{ width: "100%" }}
        mode="outlined"
        label="Date"
        value={date}
        onChangeText={(text) => setDate(text)}
      />
      <TextInput
        style={{ width: "100%" }}
        mode="outlined"
        label="Vehicle No"
        value={vehicleNo}
        onChangeText={(text) => setVehicleNo(text)}
      />
      <View style={{ marginVertical: 8 }}>
        <RadioButton.Group
          onValueChange={(value) => setVehicleSize(value)}
          value={vehicleSize}
        >
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                alignItems: "center",
              }}
            >
              <Text>Small</Text>
              <RadioButton value="Small" />
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                alignItems: "center",
              }}
            >
              <Text>Medium</Text>
              <RadioButton value="Medium" />
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                alignItems: "center",
              }}
            >
              <Text>Large</Text>
              <RadioButton value="Large" />
            </View>
          </View>
        </RadioButton.Group>
      </View>
      <Button
        disabled={vehicleNo === "" || date === ""}
        mode="contained"
        onPress={() => onSubmit()}
        style={{ width: "100%", padding: 8, marginTop: 32 }}
      >
        Add Entry Point
      </Button>
      <Button
        mode="outlined"
        onPress={() => {
          setVehicleNo("");
          setVehicleSize("Small");
          setDate(moment().format("DD/MM/YYYY hh:mm:ss A"));
        }}
        style={{ width: "100%", padding: 8, marginTop: 8 }}
      >
        Clear
      </Button>
    </View>
  );
};

export default RegistrationScreen;
