import React, { useContext, useState, useEffect } from "react";
import { View } from "react-native";
import { Button, Text, TextInput } from "react-native-paper";
const moment = require("moment");

import { ParkingContext } from "../contexts/ParkingContext";

const CheckoutFormScreen = ({ route, navigation }) => {
  const { slotDetails } = route.params;
  const {
    currentGate,
    entryPointState,
    slotDispatch,
    recentTransactionDispatch,
  } = useContext(ParkingContext);

  const flatRate = 40;
  const [dateCheckout, setDateCheckout] = useState(
    moment().format("DD/MM/YYYY hh:mm:ss A")
  );
  const [perHour, setPerHour] = useState(20);
  const [parkingDuration, setParkingDuration] = useState(0);
  const [extraHours, setExtraHours] = useState(0);
  const [additionalHoursCharge, setAdditionalHoursCharge] = useState(0);
  const [parkingDurationComputed, setParkingDurationComputed] = useState(0);
  const [daysPenalty, setDaysPenalty] = useState(0);
  const [penalty, setPenalty] = useState(0);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    const perHourType =
      slotDetails.size === "Small"
        ? 20
        : slotDetails.size === "Medium"
        ? 60
        : 100;

    setPerHour(perHourType);

    const enteredDate = moment(slotDetails.date, "DD/MM/YYYY hh:mm:ss A");
    const exitDate = moment(dateCheckout, "DD/MM/YYYY hh:mm:ss A");
    const seconds = exitDate.diff(enteredDate, "seconds");
    const calculatedHours = seconds / 3600;

    setParkingDuration(calculatedHours.toFixed(1));
    const totalHours = Math.round(calculatedHours);
    setParkingDurationComputed(totalHours);

    setExtraHours(totalHours > 3 ? totalHours - 3 : 0);
    const additionalTotal = totalHours > 3 ? (totalHours - 3) * perHourType : 0;
    setAdditionalHoursCharge(additionalTotal);

    const days = (totalHours / 24).toFixed();
    setDaysPenalty(days >= 1 ? days : 0);
    const penaltyTotal = days >= 1 ? days * 5000 : 0;
    setPenalty(penaltyTotal);

    setTotal(flatRate + additionalTotal + penaltyTotal);

  }, [route]);

  function getGateName(id) {
    return entryPointState.filter((gate) => gate.id === id)[0].name;
  }

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text variant="headlineSmall" style={{ marginBottom: 32 }}>
        Checkout Details
      </Text>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Gate Entered"
          value={getGateName(slotDetails.gateID)}
          editable={false}
        />
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Date Entered"
          value={slotDetails.date}
          editable={false}
        />
      </View>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Gate Exit"
          value={getGateName(currentGate)}
          editable={false}
        />
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Date Exit"
          value={dateCheckout}
          editable={false}
        />
      </View>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Parking Slot"
          value={slotDetails.name}
          editable={false}
        />
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Size"
          value={slotDetails.size}
          editable={false}
        />
      </View>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Vehicle No"
          value={slotDetails.vehicleNo}
          editable={false}
        />
        <TextInput
          style={{ flex: 1, margin: 4 }}
          mode="outlined"
          label="Vehicle Size"
          value={slotDetails.vehicleSize}
          editable={false}
        />
      </View>
      <View style={{ width: "100%", marginTop: 16, paddingHorizontal: 16 }}>
        <Text style={{ textAlign: "right" }} variant="bodyLarge">
          Flat Rate (3 HRS): {flatRate}
        </Text>
        <Text style={{ textAlign: "right" }} variant="bodyLarge">
          Per Hour: {perHour}
        </Text>
        <Text style={{ textAlign: "right", marginTop: 16 }} variant="bodyLarge">
          Parking Duration: {parkingDuration} = {parkingDurationComputed}
        </Text>
        <Text style={{ textAlign: "right" }} variant="bodyLarge">
          Additional Hours Charge: {extraHours} * {perHour} ={" "}
          {additionalHoursCharge}
        </Text>
        <Text style={{ textAlign: "right" }} variant="bodyLarge">
          24 hour Penalty: {daysPenalty} * 5000 = {penalty}
        </Text>
        <Text
          style={{ textAlign: "right", marginTop: 16 }}
          variant="titleLarge"
        >
          Total: {total.toFixed(2)}
        </Text>
      </View>
      <Button
        mode="contained"
        onPress={() => {
          recentTransactionDispatch({
            type: "ADD_TRANSACTION",
            gateEntered: slotDetails.gateID,
            dateEntered: slotDetails.date,
            gateExit: currentGate,
            dateExit: dateCheckout,
            slotID: slotDetails.id,
            slotName: slotDetails.name,
            slotSize: slotDetails.size,
            vehicleNo: slotDetails.vehicleNo,
            vehicleSize: slotDetails.vehicleSize,
          });

          slotDispatch({ type: "DELETE_SLOT_VEHICLE", id: slotDetails.id });

          navigation.goBack();
        }}
        style={{ width: "100%", padding: 8, marginTop: 32 }}
      >
        Checkout
      </Button>
      <Button
        mode="outlined"
        onPress={() => navigation.goBack()}
        style={{ width: "100%", padding: 8, marginTop: 8 }}
      >
        Cancel
      </Button>
    </View>
  );
};

export default CheckoutFormScreen;
