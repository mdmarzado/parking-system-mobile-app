import React, { useContext, useRef } from "react";
import { View } from "react-native";
import { Button, Text } from "react-native-paper";

import { ParkingContext } from "../contexts/ParkingContext";

const HomeScreen = ({ navigation }) => {
  const { currentGate, setCurrentGate, entryPointState } =
    useContext(ParkingContext);
  const entryPointModalRef = useRef(null);

  const onOpen = () => {
    entryPointModalRef.current?.open();
  };

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <Text variant="headlineSmall" style={{ marginBottom: 32 }}>
        Select Entry Point
      </Text>
      {entryPointState.map((entryPoint) => (
        <Button
          key={entryPoint.id}
          mode="contained"
          onPress={() => {
            setCurrentGate(entryPoint.id);
            navigation.navigate("TabNavigation");
          }}
          style={{ width: "100%", padding: 8, marginBottom: 8 }}
        >
          {entryPoint.name}
        </Button>
      ))}
      <Button
        mode="contained"
        buttonColor="gray"
        onPress={() => navigation.navigate("Entry Point Form")}
        style={{ width: "100%", padding: 8, marginTop: 32 }}
      >
        Add Entry Point
      </Button>
    </View>
  );
};

export default HomeScreen;
