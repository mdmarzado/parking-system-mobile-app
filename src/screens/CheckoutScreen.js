import React, { useContext } from "react";
import { ScrollView, TouchableOpacity } from "react-native";
import { List } from "react-native-paper";

import { ParkingContext } from "../contexts/ParkingContext";

const CheckoutScreen = ({ navigation }) => {
  const { entryPointState, slotState } = useContext(ParkingContext);

  function getGateName(id) {
    return entryPointState.filter((gate) => gate.id === id)[0].name;
  }

  return (
    <ScrollView>
      <List.Section>
        <List.Accordion title="Register" expanded>
          {slotState
            .filter((registered) => registered.vehicleNo !== "")
            .map((slot) => (
              <TouchableOpacity
                key={slot.id}
                onPress={() =>
                  navigation.navigate("Checkout Form", { slotDetails: slot })
                }
              >
                <List.Item
                  style={{
                    backgroundColor: "#fafafa",
                    margin: 8,
                  }}
                  title={slot.name}
                  description={`${getGateName(slot.gateID)} - ${slot.vehicleNo} - ${slot.vehicleSize} - ${slot.date}`}
                />
              </TouchableOpacity>
            ))}
        </List.Accordion>
      </List.Section>
    </ScrollView>
  );
};

export default CheckoutScreen;
