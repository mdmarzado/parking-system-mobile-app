### Install Expo CLI

[Expo CLI](https://docs.expo.dev/workflow/expo-cli/) is a command-line tool that is the primary interface between a developer and other Expo tools. You are going to use it for different tasks in the development life cycle of your project such as serving the project in development, viewing logs, opening the app on an emulator or a physical device, and so on.

```
npm i -g expo-cli
```

### Initializing the project
To initialize the project, navigate to project directory and install packages

```
cd parking-lot-mobile-app && npm i 
```

### Starting the development server

Start the development server by running the following command:

```
npx expo start
```
