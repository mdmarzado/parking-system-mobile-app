import React from "react";
import { Provider as PaperProvider } from "react-native-paper";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import AppNavigation from "./src/layout/AppNavigation";

import ParkingContextProvider from "./src/contexts/ParkingContext";

import HomeScreen from "./src/screens/HomeScreen";
import ParkingScreen from "./src/screens/ParkingScreen";
import RegistrationScreen from "./src/screens/RegistrationScreen";
import CheckoutScreen from "./src/screens/CheckoutScreen";
import EntryPointFormScreen from "./src/screens/EntryPointFormScreen";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function TabNavigation() {
  return (
    <Tab.Navigator initialRouteName="Parking">
      <Tab.Screen name="Parking" component={ParkingScreen} />
      <Tab.Screen name="Registration" component={RegistrationScreen} />
      <Tab.Screen name="Checkout" component={CheckoutScreen} />
    </Tab.Navigator>
  );
}

export default function App() {
  return (
    <ParkingContextProvider>
      <PaperProvider>
        <AppNavigation />
      </PaperProvider>
    </ParkingContextProvider>
  );
}
